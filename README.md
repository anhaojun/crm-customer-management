# CRM客户管理系统

#### 介绍
大三下学期java实训作业，crm关系管理系统，开发环境：window10，集成开发工具IDEA，编译环境：JDK1.8,web服务器：tomacat 9.0.361，数据库MySQL 5.7

#### 软件架构
软件架构说明


#### 安装教程

1.  导入数据库资源  w.sql
2.  导入文件，可以通过网络拉取也可以下载之后导入，通过maven进行依赖包的下载，设置你的依赖路径
3.  配置tomacat服务器环境，运行。

#### 使用说明

- 系统开发环境以及版本
  - 操作系统： Windows_10
  - 集成开发工具： IDEA2020
  - 编译环境：JDK_1.8
  - Web服务器：Tomcat_9.0
  - 数据库：MySQL_5.7.23

- 系统框架
  - spring框架
  - springmvc框架
  - mybatis框架
  - Logback日志框架
  - 安全验证框架
  - maven框架
  - layui前端框架
  - shiro安全框架

- 系统关键性技术
  - Spring+Springmvc+Mybatis三大框架
  - Ajax技术
  - springmvc文件上传
  - shiro安全框架
  - Redis缓存
  - JavaMail邮件
  - 基于aop切面的日志管理
  - Layui前端框架
  - 登录验证码
  - 富文本输入框
  - md5加密加盐

#### 后期修改
- 进行前后端分离开发，将jsp分离出来
- 添加swagger2 框架，方便接口观看

#### 参与贡献

##### w组全体成员

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
