/*
 Navicat MySQL Data Transfer

 Source Server         : 数据库
 Source Server Type    : MySQL
 Source Server Version : 50716
 Source Host           : localhost:3306
 Source Schema         : crm

 Target Server Type    : MySQL
 Target Server Version : 50716
 File Encoding         : 65001

 Date: 18/05/2021 13:04:05
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for datadictionary
-- ----------------------------
DROP TABLE IF EXISTS `datadictionary`;
CREATE TABLE `datadictionary`  (
  `dicdir_id` int(11) NOT NULL AUTO_INCREMENT,
  `sn` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `intro` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典目录简介',
  PRIMARY KEY (`dicdir_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of datadictionary
-- ----------------------------
INSERT INTO `datadictionary` VALUES (8, 'job', '职业', '做什么的');
INSERT INTO `datadictionary` VALUES (9, 'source', '来源', '客户来源渠道');
INSERT INTO `datadictionary` VALUES (10, 'IntentionDegree', '意向程度', '有多么想入坑');
INSERT INTO `datadictionary` VALUES (11, 'subject', '学科', '学科分类');
INSERT INTO `datadictionary` VALUES (12, 'size', '首款类型', '学费收款方式');

-- ----------------------------
-- Table structure for department
-- ----------------------------
DROP TABLE IF EXISTS `department`;
CREATE TABLE `department`  (
  `dept_id` int(11) NOT NULL AUTO_INCREMENT,
  `sn` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `manager` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `parent` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `state` int(2) NULL DEFAULT NULL,
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of department
-- ----------------------------
INSERT INTO `department` VALUES (18, '研发部', '研发部', NULL, NULL, NULL);
INSERT INTO `department` VALUES (19, '测试部', '测试部', NULL, NULL, NULL);
INSERT INTO `department` VALUES (20, '人事部', '人事部', NULL, NULL, NULL);
INSERT INTO `department` VALUES (21, '123', '123', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for detaildirectory
-- ----------------------------
DROP TABLE IF EXISTS `detaildirectory`;
CREATE TABLE `detaildirectory`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dir_id` int(11) NULL DEFAULT NULL,
  `det_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of detaildirectory
-- ----------------------------
INSERT INTO `detaildirectory` VALUES (4, 3, 4);
INSERT INTO `detaildirectory` VALUES (10, 8, 10);
INSERT INTO `detaildirectory` VALUES (11, 8, 11);
INSERT INTO `detaildirectory` VALUES (12, 8, 12);
INSERT INTO `detaildirectory` VALUES (13, 8, 13);
INSERT INTO `detaildirectory` VALUES (14, 8, 14);
INSERT INTO `detaildirectory` VALUES (15, 9, 15);
INSERT INTO `detaildirectory` VALUES (16, 9, 16);
INSERT INTO `detaildirectory` VALUES (17, 9, 17);
INSERT INTO `detaildirectory` VALUES (18, 9, 18);
INSERT INTO `detaildirectory` VALUES (19, 9, 19);

-- ----------------------------
-- Table structure for dirdetial
-- ----------------------------
DROP TABLE IF EXISTS `dirdetial`;
CREATE TABLE `dirdetial`  (
  `dirdetial_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `sequence` int(10) NULL DEFAULT NULL,
  PRIMARY KEY (`dirdetial_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dirdetial
-- ----------------------------
INSERT INTO `dirdetial` VALUES (10, '学生', 1);
INSERT INTO `dirdetial` VALUES (11, '老师', 2);
INSERT INTO `dirdetial` VALUES (12, '司机', 3);
INSERT INTO `dirdetial` VALUES (13, '老板', 1);
INSERT INTO `dirdetial` VALUES (14, '秘书', 3);
INSERT INTO `dirdetial` VALUES (15, 'QQ', 4);
INSERT INTO `dirdetial` VALUES (16, '接头小广告', 1);
INSERT INTO `dirdetial` VALUES (17, '网络搜索', 3);
INSERT INTO `dirdetial` VALUES (18, '朋友圈', 5);
INSERT INTO `dirdetial` VALUES (19, '电话联系', 6);

-- ----------------------------
-- Table structure for employee
-- ----------------------------
DROP TABLE IF EXISTS `employee`;
CREATE TABLE `employee`  (
  `emp_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `realName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tel` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `dept_id` int(11) NULL DEFAULT NULL,
  `inputTime` datetime(0) NULL DEFAULT NULL,
  `state` int(255) NULL DEFAULT NULL,
  `admin` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `birthDay` datetime(0) NULL DEFAULT NULL,
  `sex` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`emp_id`, `username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 399 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of employee
-- ----------------------------
INSERT INTO `employee` VALUES (1, 'admin', '张三', '37cc4f8e788c409bc31abe00788bd468', '135465', '123123123', 18, '2021-05-04 23:56:12', 1, '1', '2021-05-04 02:13:51', '男');
INSERT INTO `employee` VALUES (2, 'test1', 'test1', '37cc4f8e788c409bc31abe00788bd468', '123123123', '23123123', 20, '2021-05-10 15:00:08', 1, NULL, '2021-05-10 15:00:44', '男');
INSERT INTO `employee` VALUES (299, '201842299', '张三2', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332381', '19765652@qq.com', 19, '2020-04-22 16:00:00', 1, NULL, '1979-02-03 16:00:00', '女');
INSERT INTO `employee` VALUES (300, '201842300', '张三3', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332381', '19765653@qq.com', 20, '2020-05-20 16:00:00', 1, NULL, '1980-02-16 16:00:00', '女');
INSERT INTO `employee` VALUES (302, '201842302', '张三5', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332382', '19765655@qq.com', 19, '2020-07-15 16:00:00', 1, NULL, '1982-03-13 16:00:00', '女');
INSERT INTO `employee` VALUES (313, '201842299', '张三2', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332381', '19765652@qq.com', 19, '2020-04-22 16:00:00', 1, NULL, '1979-02-03 16:00:00', '女');
INSERT INTO `employee` VALUES (314, '201842300', '张三3', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332381', '19765653@qq.com', 20, '2020-05-20 16:00:00', 1, NULL, '1980-02-16 16:00:00', '女');
INSERT INTO `employee` VALUES (316, '201842302', '张三5', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332382', '19765655@qq.com', 19, '2020-07-15 16:00:00', 1, NULL, '1982-03-13 16:00:00', '女');
INSERT INTO `employee` VALUES (317, '201842303', '张三6', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332381', '19765656@qq.com', 20, '2020-08-12 16:00:00', 1, NULL, '1983-03-26 16:00:00', '女');
INSERT INTO `employee` VALUES (319, '201842305', '张三8', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332381', '19765658@qq.com', 19, '2020-10-07 16:00:00', 1, NULL, '1985-04-20 16:00:00', '女');
INSERT INTO `employee` VALUES (320, '201842306', '张三9', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332384', '19765659@qq.com', 20, '2020-11-04 16:00:00', 1, NULL, '1986-05-03 16:00:00', '女');
INSERT INTO `employee` VALUES (321, '201842307', '张三10', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332381', '19765660@qq.com', 18, '2020-12-02 16:00:00', 1, NULL, '1987-05-16 15:00:00', '男');
INSERT INTO `employee` VALUES (323, '201842309', '张三12', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332381', '19765662@qq.com', 20, '2021-01-27 16:00:00', 1, NULL, '1989-06-10 15:00:00', '女');
INSERT INTO `employee` VALUES (324, '201842310', '张三13', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332386', '19765663@qq.com', 18, '2021-02-24 16:00:00', 1, NULL, '1990-06-23 15:00:00', '男');
INSERT INTO `employee` VALUES (325, '201842311', '张三14', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332381', '19765664@qq.com', 19, '2021-03-24 16:00:00', 1, NULL, '1991-07-06 15:00:00', '女');
INSERT INTO `employee` VALUES (326, '201842298', '张三1', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332380', '19765651@qq.com', 18, '2020-03-25 16:00:00', 1, NULL, '1978-01-21 16:00:00', '男');
INSERT INTO `employee` VALUES (327, '201842299', '张三2', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332381', '19765652@qq.com', 19, '2020-04-22 16:00:00', 1, NULL, '1979-02-03 16:00:00', '女');
INSERT INTO `employee` VALUES (328, '201842300', '张三3', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332381', '19765653@qq.com', 20, '2020-05-20 16:00:00', 1, NULL, '1980-02-16 16:00:00', '女');
INSERT INTO `employee` VALUES (329, '201842301', '张三4', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332381', '19765654@qq.com', 18, '2020-06-17 16:00:00', 1, NULL, '1981-02-28 16:00:00', '男');
INSERT INTO `employee` VALUES (330, '201842302', '张三5', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332382', '19765655@qq.com', 19, '2020-07-15 16:00:00', 1, NULL, '1982-03-13 16:00:00', '女');
INSERT INTO `employee` VALUES (331, '201842303', '张三6', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332381', '19765656@qq.com', 20, '2020-08-12 16:00:00', 1, NULL, '1983-03-26 16:00:00', '女');
INSERT INTO `employee` VALUES (332, '201842304', '张三7', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332383', '19765657@qq.com', 18, '2020-09-09 16:00:00', 1, NULL, '1984-04-07 16:00:00', '男');
INSERT INTO `employee` VALUES (333, '201842305', '张三8', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332381', '19765658@qq.com', 19, '2020-10-07 16:00:00', 1, NULL, '1985-04-20 16:00:00', '女');
INSERT INTO `employee` VALUES (334, '201842306', '张三9', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332384', '19765659@qq.com', 20, '2020-11-04 16:00:00', 1, NULL, '1986-05-03 16:00:00', '女');
INSERT INTO `employee` VALUES (335, '201842307', '张三10', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332381', '19765660@qq.com', 18, '2020-12-02 16:00:00', 1, NULL, '1987-05-16 15:00:00', '男');
INSERT INTO `employee` VALUES (336, '201842308', '张三11', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332385', '19765661@qq.com', 19, '2020-12-30 16:00:00', 1, NULL, '1988-05-28 15:00:00', '女');
INSERT INTO `employee` VALUES (337, '201842309', '张三12', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332381', '19765662@qq.com', 20, '2021-01-27 16:00:00', 1, NULL, '1989-06-10 15:00:00', '女');
INSERT INTO `employee` VALUES (338, '201842310', '张三13', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332386', '19765663@qq.com', 18, '2021-02-24 16:00:00', 1, NULL, '1990-06-23 15:00:00', '男');
INSERT INTO `employee` VALUES (339, '201842311', '张三14', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332381', '19765664@qq.com', 19, '2021-03-24 16:00:00', 1, NULL, '1991-07-06 15:00:00', '女');
INSERT INTO `employee` VALUES (340, '201842298', '张三1', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332380', '19765651@qq.com', 18, '2020-03-25 16:00:00', 1, NULL, '1978-01-21 16:00:00', '男');
INSERT INTO `employee` VALUES (341, '201842299', '张三2', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332381', '19765652@qq.com', 19, '2020-04-22 16:00:00', 1, NULL, '1979-02-03 16:00:00', '女');
INSERT INTO `employee` VALUES (342, '201842300', '张三3', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332381', '19765653@qq.com', 20, '2020-05-20 16:00:00', 1, NULL, '1980-02-16 16:00:00', '女');
INSERT INTO `employee` VALUES (343, '201842301', '张三4', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332381', '19765654@qq.com', 18, '2020-06-17 16:00:00', 1, NULL, '1981-02-28 16:00:00', '男');
INSERT INTO `employee` VALUES (344, '201842302', '张三5', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332382', '19765655@qq.com', 19, '2020-07-15 16:00:00', 1, NULL, '1982-03-13 16:00:00', '女');
INSERT INTO `employee` VALUES (345, '201842303', '张三6', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332381', '19765656@qq.com', 20, '2020-08-12 16:00:00', 1, NULL, '1983-03-26 16:00:00', '女');
INSERT INTO `employee` VALUES (346, '201842304', '张三7', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332383', '19765657@qq.com', 18, '2020-09-09 16:00:00', 1, NULL, '1984-04-07 16:00:00', '男');
INSERT INTO `employee` VALUES (347, '201842305', '张三8', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332381', '19765658@qq.com', 19, '2020-10-07 16:00:00', 1, NULL, '1985-04-20 16:00:00', '女');
INSERT INTO `employee` VALUES (348, '201842306', '张三9', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332384', '19765659@qq.com', 20, '2020-11-04 16:00:00', 1, NULL, '1986-05-03 16:00:00', '女');
INSERT INTO `employee` VALUES (349, '201842307', '张三10', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332381', '19765660@qq.com', 18, '2020-12-02 16:00:00', 1, NULL, '1987-05-16 15:00:00', '男');
INSERT INTO `employee` VALUES (350, '201842308', '张三11', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332385', '19765661@qq.com', 19, '2020-12-30 16:00:00', 1, NULL, '1988-05-28 15:00:00', '女');
INSERT INTO `employee` VALUES (351, '201842309', '张三12', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332381', '19765662@qq.com', 20, '2021-01-27 16:00:00', 1, NULL, '1989-06-10 15:00:00', '女');
INSERT INTO `employee` VALUES (352, '201842310', '张三13', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332386', '19765663@qq.com', 18, '2021-02-24 16:00:00', 1, NULL, '1990-06-23 15:00:00', '男');
INSERT INTO `employee` VALUES (353, '201842311', '张三14', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332381', '19765664@qq.com', 19, '2021-03-24 16:00:00', 1, NULL, '1991-07-06 15:00:00', '女');
INSERT INTO `employee` VALUES (354, '201842298', '张三1', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332380', '19765651@qq.com', 18, '2020-03-25 16:00:00', 1, NULL, '1978-01-21 16:00:00', '男');
INSERT INTO `employee` VALUES (355, '201842299', '张三2', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332381', '19765652@qq.com', 19, '2020-04-22 16:00:00', 1, NULL, '1979-02-03 16:00:00', '女');
INSERT INTO `employee` VALUES (356, '201842300', '张三3', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332381', '19765653@qq.com', 20, '2020-05-20 16:00:00', 1, NULL, '1980-02-16 16:00:00', '女');
INSERT INTO `employee` VALUES (357, '201842301', '张三4', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332381', '19765654@qq.com', 18, '2020-06-17 16:00:00', 1, NULL, '1981-02-28 16:00:00', '男');
INSERT INTO `employee` VALUES (358, '201842302', '张三5', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332382', '19765655@qq.com', 19, '2020-07-15 16:00:00', 1, NULL, '1982-03-13 16:00:00', '女');
INSERT INTO `employee` VALUES (359, '201842303', '张三6', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332381', '19765656@qq.com', 20, '2020-08-12 16:00:00', 1, NULL, '1983-03-26 16:00:00', '女');
INSERT INTO `employee` VALUES (360, '201842304', '张三7', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332383', '19765657@qq.com', 18, '2020-09-09 16:00:00', 1, NULL, '1984-04-07 16:00:00', '男');
INSERT INTO `employee` VALUES (361, '201842305', '张三8', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332381', '19765658@qq.com', 19, '2020-10-07 16:00:00', 1, NULL, '1985-04-20 16:00:00', '女');
INSERT INTO `employee` VALUES (362, '201842306', '张三9', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332384', '19765659@qq.com', 20, '2020-11-04 16:00:00', 1, NULL, '1986-05-03 16:00:00', '女');
INSERT INTO `employee` VALUES (363, '201842307', '张三10', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332381', '19765660@qq.com', 18, '2020-12-02 16:00:00', 1, NULL, '1987-05-16 15:00:00', '男');
INSERT INTO `employee` VALUES (364, '201842308', '张三11', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332385', '19765661@qq.com', 19, '2020-12-30 16:00:00', 1, NULL, '1988-05-28 15:00:00', '女');
INSERT INTO `employee` VALUES (365, '201842309', '张三12', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332381', '19765662@qq.com', 20, '2021-01-27 16:00:00', 1, NULL, '1989-06-10 15:00:00', '女');
INSERT INTO `employee` VALUES (366, '201842310', '张三13', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332386', '19765663@qq.com', 18, '2021-02-24 16:00:00', 1, NULL, '1990-06-23 15:00:00', '男');
INSERT INTO `employee` VALUES (367, '201842311', '张三14', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332381', '19765664@qq.com', 19, '2021-03-24 16:00:00', 1, NULL, '1991-07-06 15:00:00', '女');
INSERT INTO `employee` VALUES (368, '201842298', '张三1', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332380', '19765651@qq.com', 18, '2020-03-25 16:00:00', 1, NULL, '1978-01-21 16:00:00', '男');
INSERT INTO `employee` VALUES (369, '201842299', '张三2', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332381', '19765652@qq.com', 19, '2020-04-22 16:00:00', 1, NULL, '1979-02-03 16:00:00', '女');
INSERT INTO `employee` VALUES (370, '201842300', '张三3', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332381', '19765653@qq.com', 20, '2020-05-20 16:00:00', 1, NULL, '1980-02-16 16:00:00', '女');
INSERT INTO `employee` VALUES (371, '201842301', '张三4', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332381', '19765654@qq.com', 18, '2020-06-17 16:00:00', 1, NULL, '1981-02-28 16:00:00', '男');
INSERT INTO `employee` VALUES (372, '201842302', '张三5', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332382', '19765655@qq.com', 19, '2020-07-15 16:00:00', 1, NULL, '1982-03-13 16:00:00', '女');
INSERT INTO `employee` VALUES (373, '201842303', '张三6', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332381', '19765656@qq.com', 20, '2020-08-12 16:00:00', 1, NULL, '1983-03-26 16:00:00', '女');
INSERT INTO `employee` VALUES (374, '201842304', '张三7', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332383', '19765657@qq.com', 18, '2020-09-09 16:00:00', 1, NULL, '1984-04-07 16:00:00', '男');
INSERT INTO `employee` VALUES (375, '201842305', '张三8', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332381', '19765658@qq.com', 19, '2020-10-07 16:00:00', 1, NULL, '1985-04-20 16:00:00', '女');
INSERT INTO `employee` VALUES (376, '201842306', '张三9', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332384', '19765659@qq.com', 20, '2020-11-04 16:00:00', 1, NULL, '1986-05-03 16:00:00', '女');
INSERT INTO `employee` VALUES (377, '201842307', '张三10', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332381', '19765660@qq.com', 18, '2020-12-02 16:00:00', 1, NULL, '1987-05-16 15:00:00', '男');
INSERT INTO `employee` VALUES (378, '201842308', '张三11', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332385', '19765661@qq.com', 19, '2020-12-30 16:00:00', 1, NULL, '1988-05-28 15:00:00', '女');
INSERT INTO `employee` VALUES (379, '201842309', '张三12', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332381', '19765662@qq.com', 20, '2021-01-27 16:00:00', 1, NULL, '1989-06-10 15:00:00', '女');
INSERT INTO `employee` VALUES (380, '201842310', '张三13', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332386', '19765663@qq.com', 18, '2021-02-24 16:00:00', 1, NULL, '1990-06-23 15:00:00', '男');
INSERT INTO `employee` VALUES (381, '201842311', '张三14', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332381', '19765664@qq.com', 19, '2021-03-24 16:00:00', 1, NULL, '1991-07-06 15:00:00', '女');
INSERT INTO `employee` VALUES (383, '123123123', '123123123', 'bcbfacabae00b238a2e1b4db63dd7d70', '21312312', '123123123', 19, '2021-05-12 07:43:15', 1, NULL, '2021-05-23 16:00:00', NULL);
INSERT INTO `employee` VALUES (384, '201842298', '张三1', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332380', '19765651@qq.com', 18, '2020-03-25 16:00:00', 1, NULL, '1978-01-21 16:00:00', '男');
INSERT INTO `employee` VALUES (385, '201842299', '张三2', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332381', '19765652@qq.com', 19, '2020-04-22 16:00:00', 1, NULL, '1979-02-03 16:00:00', '女');
INSERT INTO `employee` VALUES (386, '201842300', '张三3', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332381', '19765653@qq.com', 20, '2020-05-20 16:00:00', 1, NULL, '1980-02-16 16:00:00', '女');
INSERT INTO `employee` VALUES (387, '201842301', '张三4', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332381', '19765654@qq.com', 18, '2020-06-17 16:00:00', 1, NULL, '1981-02-28 16:00:00', '男');
INSERT INTO `employee` VALUES (388, '201842302', '张三5', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332382', '19765655@qq.com', 19, '2020-07-15 16:00:00', 1, NULL, '1982-03-13 16:00:00', '女');
INSERT INTO `employee` VALUES (389, '201842303', '张三6', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332381', '19765656@qq.com', 20, '2020-08-12 16:00:00', 1, NULL, '1983-03-26 16:00:00', '女');
INSERT INTO `employee` VALUES (390, '201842304', '张三7', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332383', '19765657@qq.com', 18, '2020-09-09 16:00:00', 1, NULL, '1984-04-07 16:00:00', '男');
INSERT INTO `employee` VALUES (391, '201842305', '张三8', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332381', '19765658@qq.com', 19, '2020-10-07 16:00:00', 1, NULL, '1985-04-20 16:00:00', '女');
INSERT INTO `employee` VALUES (392, '201842306', '张三9', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332384', '19765659@qq.com', 20, '2020-11-04 16:00:00', 1, NULL, '1986-05-03 16:00:00', '女');
INSERT INTO `employee` VALUES (393, '201842307', '张三10', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332381', '19765660@qq.com', 18, '2020-12-02 16:00:00', 1, NULL, '1987-05-16 15:00:00', '男');
INSERT INTO `employee` VALUES (394, '201842308', '张三11', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332385', '19765661@qq.com', 19, '2020-12-30 16:00:00', 1, NULL, '1988-05-28 15:00:00', '女');
INSERT INTO `employee` VALUES (395, '201842309', '张三12', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332381', '19765662@qq.com', 20, '2021-01-27 16:00:00', 1, NULL, '1989-06-10 15:00:00', '女');
INSERT INTO `employee` VALUES (396, '201842310', '张三13', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332386', '19765663@qq.com', 18, '2021-02-24 16:00:00', 1, NULL, '1990-06-23 15:00:00', '男');
INSERT INTO `employee` VALUES (397, '201842311', '张三14', 'bcbfacabae00b238a2e1b4db63dd7d70', '13458332381', '19765664@qq.com', 19, '2021-03-24 16:00:00', 1, NULL, '1991-07-06 15:00:00', '女');
INSERT INTO `employee` VALUES (398, '23123', '123123', 'bcbfacabae00b238a2e1b4db63dd7d70', '3123', '12312', 21, '2021-05-12 08:25:26', 1, NULL, '2021-05-25 16:00:00', '女');

-- ----------------------------
-- Table structure for emprole
-- ----------------------------
DROP TABLE IF EXISTS `emprole`;
CREATE TABLE `emprole`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) NULL DEFAULT NULL,
  `role_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 31 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of emprole
-- ----------------------------
INSERT INTO `emprole` VALUES (4, 299, 12);
INSERT INTO `emprole` VALUES (5, 382, 5);
INSERT INTO `emprole` VALUES (6, 382, 2);
INSERT INTO `emprole` VALUES (7, 383, 2);
INSERT INTO `emprole` VALUES (8, 383, 5);
INSERT INTO `emprole` VALUES (9, 383, 17);
INSERT INTO `emprole` VALUES (16, 343, 17);
INSERT INTO `emprole` VALUES (17, 343, 5);
INSERT INTO `emprole` VALUES (18, 343, 2);
INSERT INTO `emprole` VALUES (24, 398, 17);
INSERT INTO `emprole` VALUES (25, 398, 5);
INSERT INTO `emprole` VALUES (26, 398, 2);
INSERT INTO `emprole` VALUES (27, 398, 1);
INSERT INTO `emprole` VALUES (28, 2, 2);
INSERT INTO `emprole` VALUES (29, 1, 1);
INSERT INTO `emprole` VALUES (30, 1, 2);

-- ----------------------------
-- Table structure for followhistory
-- ----------------------------
DROP TABLE IF EXISTS `followhistory`;
CREATE TABLE `followhistory`  (
  `follow_id` int(11) NOT NULL AUTO_INCREMENT,
  `traceTime` datetime(0) NULL DEFAULT NULL,
  `traceDetails` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `traceType` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `traceResult` int(11) NULL DEFAULT NULL,
  `customer_id` int(11) NULL DEFAULT NULL,
  `inputUser` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `type` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`follow_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of followhistory
-- ----------------------------
INSERT INTO `followhistory` VALUES (5, '2021-05-14 16:00:00', '我就不告诉你', '下午茶交流', 0, 13, 'test1', 0);
INSERT INTO `followhistory` VALUES (6, '2021-05-05 16:00:00', '就不告诉你', '上门拜访', 2, 23, 'test1', 1);
INSERT INTO `followhistory` VALUES (7, '2021-04-30 16:00:00', '我就是来看看', '下午茶交流', 0, 13, 'test1', 0);
INSERT INTO `followhistory` VALUES (8, '2021-05-14 16:00:00', '奥术大师多', '下午茶交流', 3, 23, 'test1', 0);
INSERT INTO `followhistory` VALUES (9, '2021-05-16 16:00:00', '我又来了', '上门拜访', 2, 13, 'test1', 1);

-- ----------------------------
-- Table structure for handoverhis
-- ----------------------------
DROP TABLE IF EXISTS `handoverhis`;
CREATE TABLE `handoverhis`  (
  `handover_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NULL DEFAULT NULL,
  `transUser` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `transTime` datetime(0) NULL DEFAULT NULL,
  `oldSeller_id` int(11) NULL DEFAULT NULL,
  `newSeller_id` int(11) NULL DEFAULT NULL,
  `transReason` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`handover_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of handoverhis
-- ----------------------------
INSERT INTO `handoverhis` VALUES (10, 13, 'test1', '2021-05-15 13:13:36', 396, 392, '我就是试试');
INSERT INTO `handoverhis` VALUES (11, 13, 'test1', '2021-05-15 13:17:16', 392, 398, '请问恶趣味群v');
INSERT INTO `handoverhis` VALUES (12, 22, 'test1', '2021-05-15 13:20:18', 2, 2, '132132132132');
INSERT INTO `handoverhis` VALUES (13, 13, 'test1', '2021-05-16 03:02:43', 398, 361, '123123123');
INSERT INTO `handoverhis` VALUES (14, 15, 'test1', '2021-05-16 03:03:22', 2, 393, '2131231');
INSERT INTO `handoverhis` VALUES (15, 22, 'test1', '2021-05-16 03:03:41', 2, 395, '');

-- ----------------------------
-- Table structure for permission
-- ----------------------------
DROP TABLE IF EXISTS `permission`;
CREATE TABLE `permission`  (
  `per_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `resource` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `lock` int(2) NULL DEFAULT NULL COMMENT '1:正常，0:上锁不可用',
  `pid` int(2) NULL DEFAULT NULL COMMENT '父ID',
  PRIMARY KEY (`per_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of permission
-- ----------------------------
INSERT INTO `permission` VALUES (1, '部门管理', 'department.do', 1, 1);
INSERT INTO `permission` VALUES (2, '员工管理', 'employee.do', 1, 1);
INSERT INTO `permission` VALUES (3, '角色管理', 'role.do', 1, 1);
INSERT INTO `permission` VALUES (4, '权限管理', 'permission.do', 1, 1);
INSERT INTO `permission` VALUES (5, '潜在客户管理', 'potentialCustoMan.do', 1, 3);
INSERT INTO `permission` VALUES (6, '客户资源池', 'CustomerRePoll.do', 1, 3);
INSERT INTO `permission` VALUES (7, '正式客户管理', 'fmCustomerMan.do', 1, 3);
INSERT INTO `permission` VALUES (8, '数据字典管理', 'datadictionary.do', 1, 2);
INSERT INTO `permission` VALUES (9, '数据字典明细管理', 'detaileddata.do', 1, 2);
INSERT INTO `permission` VALUES (10, '潜在客户报表', 'customer.do', 1, 5);
INSERT INTO `permission` VALUES (13, '跟进历史管理', 'followHisMan.do', 1, 4);
INSERT INTO `permission` VALUES (14, '移交历史查询', 'handoverHisMan.do', 1, 4);

-- ----------------------------
-- Table structure for potentialcustomer
-- ----------------------------
DROP TABLE IF EXISTS `potentialcustomer`;
CREATE TABLE `potentialcustomer`  (
  `pc_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `age` int(11) NULL DEFAULT NULL,
  `gender` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '性别',
  `tel` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `qq` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `job_id` int(11) NULL DEFAULT NULL,
  `source_id` int(11) NULL DEFAULT NULL,
  `seller_id` int(11) NULL DEFAULT NULL,
  `inputUser_id` int(11) NULL DEFAULT NULL,
  `inputTime` datetime(0) NULL DEFAULT NULL,
  `status` int(5) NULL DEFAULT NULL,
  `positiveTime` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`pc_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of potentialcustomer
-- ----------------------------
INSERT INTO `potentialcustomer` VALUES (13, '张三', 25, '男', '1345864656', '12312312', 11, 19, 361, 2, '2021-04-29 18:10:01', -1, NULL);
INSERT INTO `potentialcustomer` VALUES (14, '李红', 34, '女', '12313123123', '123123123', 14, 18, 2, 2, '2020-09-01 18:12:36', -1, NULL);
INSERT INTO `potentialcustomer` VALUES (15, '李四三', 45, '男', '12312312312', '123123123', 12, 18, 393, 2, '2021-05-14 18:14:11', 1, NULL);
INSERT INTO `potentialcustomer` VALUES (16, '李铭', 43, '男', '12123123123', '12312312', 13, 17, 2, 2, '2021-05-14 18:14:36', -2, NULL);
INSERT INTO `potentialcustomer` VALUES (17, '小霖', 22, '男', '123123123123', '1231231231', 13, 19, 302, 2, '2021-05-14 18:15:02', 1, NULL);
INSERT INTO `potentialcustomer` VALUES (18, '小微', 20, '女', '13212312', '3123123', 10, 18, 1, 2, '2021-05-19 18:15:24', 1, NULL);
INSERT INTO `potentialcustomer` VALUES (19, '小琪', 19, '女', '3123123', '12312312', 10, 17, 2, 2, '2021-05-14 18:16:07', 0, NULL);
INSERT INTO `potentialcustomer` VALUES (20, '小祁', 21, '男', '12313123123', '12312312', 10, 15, 2, 2, '2021-04-07 18:16:39', 0, NULL);
INSERT INTO `potentialcustomer` VALUES (21, '小一麟', 22, '男', '123123123', '12312312', 10, 16, 2, 2, '2021-03-11 18:17:14', 0, NULL);
INSERT INTO `potentialcustomer` VALUES (22, '昆哥', 22, '男', '12312312', '3123123', 11, 15, 395, 2, '2021-05-14 18:17:49', 1, NULL);
INSERT INTO `potentialcustomer` VALUES (23, '肖哥哥', 25, '男', '12312312312', '12312312', 13, 18, 392, 2, '2020-08-12 18:18:32', 1, NULL);

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role`  (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `sn` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES (1, '数据管理', 'DataManager');
INSERT INTO `role` VALUES (2, '系统管理员', 'SystemManager');
INSERT INTO `role` VALUES (5, '人事专员', 'Hr specialist');
INSERT INTO `role` VALUES (17, '23212', '123123');

-- ----------------------------
-- Table structure for rolepermission
-- ----------------------------
DROP TABLE IF EXISTS `rolepermission`;
CREATE TABLE `rolepermission`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NULL DEFAULT NULL,
  `per_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 101 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of rolepermission
-- ----------------------------
INSERT INTO `rolepermission` VALUES (8, 12, 1);
INSERT INTO `rolepermission` VALUES (9, 13, 1);
INSERT INTO `rolepermission` VALUES (10, 13, 2);
INSERT INTO `rolepermission` VALUES (11, 13, 3);
INSERT INTO `rolepermission` VALUES (12, 14, 1);
INSERT INTO `rolepermission` VALUES (13, 14, 2);
INSERT INTO `rolepermission` VALUES (14, 14, 4);
INSERT INTO `rolepermission` VALUES (15, 15, 1);
INSERT INTO `rolepermission` VALUES (16, 15, 2);
INSERT INTO `rolepermission` VALUES (17, 15, 3);
INSERT INTO `rolepermission` VALUES (18, 16, 1);
INSERT INTO `rolepermission` VALUES (23, 8, 3);
INSERT INTO `rolepermission` VALUES (24, 8, 1);
INSERT INTO `rolepermission` VALUES (25, 8, 2);
INSERT INTO `rolepermission` VALUES (26, 8, 4);
INSERT INTO `rolepermission` VALUES (29, 17, 2);
INSERT INTO `rolepermission` VALUES (30, 17, 3);
INSERT INTO `rolepermission` VALUES (44, 1, 3);
INSERT INTO `rolepermission` VALUES (45, 1, 1);
INSERT INTO `rolepermission` VALUES (46, 1, 2);
INSERT INTO `rolepermission` VALUES (47, 1, 4);
INSERT INTO `rolepermission` VALUES (48, 1, 5);
INSERT INTO `rolepermission` VALUES (49, 1, 6);
INSERT INTO `rolepermission` VALUES (50, 1, 7);
INSERT INTO `rolepermission` VALUES (51, 1, 8);
INSERT INTO `rolepermission` VALUES (52, 1, 9);
INSERT INTO `rolepermission` VALUES (53, 1, 10);
INSERT INTO `rolepermission` VALUES (54, 1, 13);
INSERT INTO `rolepermission` VALUES (55, 1, 14);
INSERT INTO `rolepermission` VALUES (89, 2, 3);
INSERT INTO `rolepermission` VALUES (90, 2, 4);
INSERT INTO `rolepermission` VALUES (91, 2, 7);
INSERT INTO `rolepermission` VALUES (92, 2, 8);
INSERT INTO `rolepermission` VALUES (93, 2, 9);
INSERT INTO `rolepermission` VALUES (94, 2, 10);
INSERT INTO `rolepermission` VALUES (95, 2, 13);
INSERT INTO `rolepermission` VALUES (96, 2, 14);
INSERT INTO `rolepermission` VALUES (97, 2, 1);
INSERT INTO `rolepermission` VALUES (98, 2, 2);
INSERT INTO `rolepermission` VALUES (99, 2, 5);
INSERT INTO `rolepermission` VALUES (100, 2, 6);

-- ----------------------------
-- Table structure for systemdictionary
-- ----------------------------
DROP TABLE IF EXISTS `systemdictionary`;
CREATE TABLE `systemdictionary`  (
  `sd_id` int(11) NOT NULL AUTO_INCREMENT,
  `sn` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `intro` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`sd_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for systemdictionaryitem
-- ----------------------------
DROP TABLE IF EXISTS `systemdictionaryitem`;
CREATE TABLE `systemdictionaryitem`  (
  `sdi_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `intro` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`sdi_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
