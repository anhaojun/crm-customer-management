package com.w.crmsystem.controller.admin;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @Author Bamboo
 * @Date 2021/5/13 1:34
 * @Version 1.0
 */
@ToString
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DirdetialExample {
    private Integer dirdetailId;
    private String title;
    private Integer sequence;
    private Integer dicdirId;
}
