package com.w.crmsystem.base;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

/**
<<<<<<< HEAD
 * @Author Bamboo
=======
 *   @ClassName:  
* @Description:
 * @Author: yun
>>>>>>> 代码优化
 * @Date 2021/5/9 18:31
 * @Version 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class INITInfo {
    private HomeInfo homeInfo;
    private LogoInfo logoInfo;
    private List<MenuInfo> menuInfo;

}
